//
//  NewTeamViewController.swift
//  NWProgrammingContest
//
//  Created by Student on 3/13/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class NewTeamViewController: UIViewController {

    @IBOutlet weak var nameTF: UITextField!
    
    @IBOutlet weak var student0TF: UITextField!
    
    @IBOutlet weak var student1TF: UITextField!
    
    
    @IBOutlet weak var student2TF: UITextField!
    
    
    var school: School!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBAction func done(_ sender: Any) {
        let name = nameTF.text!
        let student0Name = student0TF.text!
        let student1Name = student1TF.text!
        let student2Name = student2TF.text!
        if nameTF.text!.count != 0 && student0TF.text!.count != 0 && student1TF.text!.count != 0 && student2TF.text!.count != 0 {
        Schools.shared.saveTeamForSelectedSchool(school:school,team: Team(name: name, students: [student0Name, student1Name, student2Name]))
        self.dismiss(animated: true, completion: nil)
        
    }
        else {
            displayMessage()
    }
    }
    
    @IBAction func cancelBTN(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    func displayMessage(){
        let alert = UIAlertController(title: "Error", message: "Please enter valid value in all text fields.",preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss" , style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
