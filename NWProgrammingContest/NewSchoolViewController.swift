//
//  NewSchoolViewController.swift
//  NWProgrammingContest
//
//  Created by Student on 3/13/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class NewSchoolViewController: UIViewController {
   
    @IBOutlet weak var NameTF: UITextField!
    
    @IBOutlet weak var CoachTF: UITextField!
    
  
    
    var AddNewSchool:School!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func done(_sender:Any){
        let schoolName = NameTF.text!
        let coachName = CoachTF.text!
        if NameTF.text! != "" && CoachTF.text! != ""{
        Schools.shared.saveSchool(name: schoolName, coach: coachName)
        self.dismiss(animated: true, completion: nil)
        }else{
            displayMessage()
        }
    }
    
    func displayMessage(){
        let alert = UIAlertController(title: "Error", message: "Please enter valid value in all text fields.",preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss" , style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func cancel(_sender:Any){
        self.dismiss(animated: true, completion: nil)

        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
